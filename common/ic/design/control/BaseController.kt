package ic.design.control


abstract class BaseController<Subject> : BaseControllerWithEnv<Subject, Unit>() {

	fun open (subject: Subject) = open(environment = Unit, subject = subject)

}