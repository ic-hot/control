package ic.design.control


abstract class BaseControllerWithEnv<Subject, Environment> : BaseStatefulControllerWithEnv<Subject, Unit, Environment>() {


	override fun snapshotState() = Unit


}