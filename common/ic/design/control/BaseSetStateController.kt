package ic.design.control


abstract class BaseSetStateController<Subject, State>
	: BaseSetStateControllerWithEnv<Subject, State, Unit>()
	, SetStateController<Subject, State>
{



}