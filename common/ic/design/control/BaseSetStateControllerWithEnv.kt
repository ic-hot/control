package ic.design.control


abstract class BaseSetStateControllerWithEnv<Subject, State, Env>
	: BaseSetStateControllerWithTransAndEnv<Subject, State, Unit, Env>()
	, SetStateControllerWithEnv<Subject, State, Env>
{


	final override fun setState (state: State, transition: Unit) {
		super<BaseSetStateControllerWithTransAndEnv>.setState(state, transition)
	}

	override fun setState(state: State) {
		super<BaseSetStateControllerWithTransAndEnv>.setState(state, Unit)
	}


	protected open fun onSetState (state: State) {}

	final override fun onSetState (state: State, transition: Unit?) {
		onSetState(state)
	}

	protected open fun onTransitState (oldState: State?, newState: State) {}

	final override fun onTransitState (oldState: State?, newState: State, transition: Unit?) {
		onTransitState(oldState = oldState, newState = newState)
	}


	protected open fun onChangeState (state: State) {}

	protected open fun onChangeState (oldState: State?, newState: State) {}

	final override fun onChangeState (oldState: State?, newState: State, transition: Unit?) {
		onChangeState(oldState = oldState, newState = newState)
		onChangeState(state = newState)
	}


}