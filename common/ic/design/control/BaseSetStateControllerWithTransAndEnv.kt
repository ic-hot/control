package ic.design.control


import ic.base.assert.assert
import ic.base.reflect.ext.className


abstract class BaseSetStateControllerWithTransAndEnv
	<Subject, State, Transition, Environment>
	: BaseStatefulControllerWithEnv<Subject, State, Environment>()
	, SetStateControllerWithTransAndEnv<Subject, State, Transition, Environment>
{


	private var isStateApplied : Boolean = false

	private var stateField : State? = null


	override fun snapshotState() : State {
		assert ("$className: No state applied") { isStateApplied }
		@Suppress("UNCHECKED_CAST")
		return stateField as State
	}


	/**
	 * Called on FIRST state apply after open
	 */
	protected open fun onInitState (state: State) {}

	/**
	 * Called on ANY EXCEPT FIRST state apply
	 */
	protected open fun onUpdateState (oldState: State, newState: State, transition: Transition) {}

	/**
	 * Called on ANY state apply
	 */
	protected open fun onTransitState (oldState: State?, newState: State, transition: Transition?) {}

	/**
	 * Called on ANY state apply
	 */
	protected open fun onSetState (state: State, transition: Transition?) {}

	/**
	 * Called on any state apply with newState != oldState
	 */
	protected open fun onChangeState (oldState: State?, newState: State, transition: Transition?) {}


	private fun initState (state: State) {
		stateField = state
		isStateApplied = true
		onInitState(state)
		onSetState(state = state, transition = null)
		onTransitState(oldState = null, newState = state, transition = null)
		onChangeState(oldState = null, newState = state, transition = null)
	}

	private fun updateState (state: State, transition: Transition) {
		assert { isStateApplied }
		@Suppress("UNCHECKED_CAST")
		val oldState = stateField as State
		stateField = state
		onSetState(state = state, transition = transition)
		onTransitState(oldState = oldState, newState = state, transition = transition)
		if (state != oldState) {
			onChangeState(
				oldState = oldState, newState = state, transition = transition
			)
		}
		onUpdateState(oldState = oldState, newState = state, transition = transition)
	}

	override fun setState (state: State, transition: Transition) {
		if (isStateApplied) {
			updateState(state, transition)
		} else {
			initState(state)
		}
	}


	final override fun onOpenWithState (state: State) {
		initState(state)
	}


	override fun internalOnClose() {
		isStateApplied = false
		stateField = null
	}


}