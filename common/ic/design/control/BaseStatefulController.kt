package ic.design.control


abstract class BaseStatefulController<Subject, State>
	: BaseStatefulControllerWithEnv<Subject, State, Unit>()
	, StatefulController<Subject, State>
{



}