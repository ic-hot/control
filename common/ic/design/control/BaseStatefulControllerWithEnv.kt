package ic.design.control


import ic.design.task.scope.BaseTaskScope


abstract class BaseStatefulControllerWithEnv<Subject, State, Environment>
	: BaseTaskScope()
	, StatefulControllerWithEnv<Subject, State, Environment>
{


	override var isOpen = false


	private var environmentField : Environment? = null

	@Suppress("UNCHECKED_CAST")
	override val environment get() = environmentField as Environment


	private var subjectField : Subject? = null

	@Suppress("UNCHECKED_CAST")
	override val subject get() = subjectField as Subject


	protected open fun onOpen() {}

	internal inline fun implementOpen (
		initSubject : () -> Subject,
		environment : Environment
	) : Subject {
		if (isOpen) close()
		this.environmentField = environment
		val subject = initSubject()
		this.subjectField = subject
		this.isOpen = true
		onOpen()
		return subject
	}

	final override fun open (environment: Environment, subject: Subject) {
		implementOpen(
			initSubject = { subject },
			environment = environment
		)
	}


	protected open fun onOpenWithState (state: State) {}

	override fun openWithState (environment: Environment, subject: Subject, state: State) {
		open(
			environment = environment,
			subject = subject
		)
		onOpenWithState(state)
	}


	protected abstract fun snapshotState() : State

	override fun getState() = snapshotState()


	protected open fun onClose() {}


	internal open fun internalOnClose() {}


	final override fun close() {
		if (!isOpen) return
		isOpen = false
		cancelTasks()
		onClose()
		internalOnClose()
		environmentField = null
		subjectField = null
		isOpen = false
	}


}