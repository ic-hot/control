package ic.design.control


interface Controller<Subject>
	: ControllerWithEnv<Subject, Unit>
	, StatefulController<Subject, Unit>
{



}