package ic.design.control


interface SetStateController<Subject, State>
	: SetStateControllerWithEnv<Subject, State, Unit>
	, SetStateControllerWithTrans<Subject, State, Unit>
{



}