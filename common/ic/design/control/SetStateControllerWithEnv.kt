package ic.design.control


import ic.ifaces.stateful.SetState


interface SetStateControllerWithEnv

	<Subject, State, Environment>

	: SetStateControllerWithTransAndEnv<Subject, State, Unit, Environment>
	, SetState<State>

{



}