package ic.design.control


interface SetStateControllerWithTrans
	<Subject, State, Transition>
	: SetStateControllerWithTransAndEnv<Subject, State, Transition, Unit>
	, StatefulController<Subject, State>
{
}