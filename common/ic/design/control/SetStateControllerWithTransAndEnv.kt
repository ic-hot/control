package ic.design.control


import ic.ifaces.stateful.SetStateWithTransition


interface SetStateControllerWithTransAndEnv

	<Subject, State, Transition, Environment>

	: StatefulControllerWithEnv<Subject, State, Environment>
	, SetStateWithTransition<State, Transition>

{



}