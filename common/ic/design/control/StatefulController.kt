package ic.design.control


interface StatefulController<Subject, State> : StatefulControllerWithEnv<Subject, State, Unit> {

	fun open (subject: Subject) {
		open(environment = Unit, subject = subject)
	}

}