package ic.design.control


import ic.ifaces.lifecycle.closeable.Closeable
import ic.ifaces.stateful.Stateful


interface StatefulControllerWithEnv<Subject, State, Environment> : Stateful<State>, Closeable {


	val subject : Subject

	val environment : Environment


	val isOpen : Boolean


	fun open (environment: Environment, subject: Subject,)

	fun openIfNeeded (environment: Environment, subject: Subject) {
		if (isOpen) return
		open(
			environment = environment,
			subject = subject
		)
	}


	fun openWithState (environment: Environment, subject: Subject, state: State, )


	fun closeIfNeeded() {
		if (!isOpen) return
		close()
	}


}