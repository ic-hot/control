package ic.design.control.alias


import ic.design.control.SetStateControllerWithTransAndEnv


typealias SetStateControllerWithEnv<Subject, State, Env> =
	SetStateControllerWithTransAndEnv<Subject, State, Unit, Env>
;