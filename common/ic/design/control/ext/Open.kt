@file:Suppress("NOTHING_TO_INLINE")


package ic.design.control.ext


import kotlin.jvm.JvmName

import ic.design.control.StatefulControllerWithEnv


@JvmName("openWithUnitEnvironment")
inline fun <Subject, State> StatefulControllerWithEnv<Subject, State, Unit>.open (subject: Subject) {
	open(environment = Unit, subject = subject)
}


@JvmName("openWithUnitSubject")
inline fun <State, Env> StatefulControllerWithEnv<Unit, State, Env>.open (environment: Env) {
	open(subject = Unit, environment = environment)
}


@JvmName("openWithUnitSubjectAndState")
inline fun <State, Env> StatefulControllerWithEnv<Unit, State, Env>.openWithState (environment: Env, state: State) {
	openWithState(environment = environment, subject = Unit, state = state)
}