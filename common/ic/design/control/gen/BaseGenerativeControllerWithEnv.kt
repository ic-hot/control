package ic.design.control.gen


import ic.design.control.BaseControllerWithEnv


abstract class BaseGenerativeControllerWithEnv<Subject, Environment>
	: BaseControllerWithEnv<Subject, Environment>()
	, GenerativeControllerWithEnv<Subject, Environment>
{


	protected abstract fun initSubject() : Subject


	override fun open (environment: Environment) : Subject {
		return implementOpen(
			initSubject = ::initSubject,
			environment = environment
		)
	}


}