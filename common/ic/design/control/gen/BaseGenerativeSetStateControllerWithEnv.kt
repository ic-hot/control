package ic.design.control.gen


import ic.design.control.BaseSetStateControllerWithEnv


abstract class BaseGenerativeSetStateControllerWithEnv
	<Subject, State, Environment>
	: BaseSetStateControllerWithEnv<Subject, State, Environment>()
	, GenerativeSetStateControllerWithEnv<Subject, State, Environment>
{


	protected abstract fun initSubject() : Subject


	override fun open (environment: Environment) : Subject {
		return implementOpen(
			initSubject = ::initSubject,
			environment = environment
		)
	}


	override fun openWithState (state: State, environment: Environment) : Subject {
		val subject = open(environment)
		onOpenWithState(state)
		return subject
	}


}