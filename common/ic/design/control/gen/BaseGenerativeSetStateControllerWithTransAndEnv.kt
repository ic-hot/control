package ic.design.control.gen


import ic.design.control.BaseSetStateControllerWithTransAndEnv


abstract class BaseGenerativeSetStateControllerWithTransAndEnv
	<Subject, State, Transition, Environment>
	: BaseSetStateControllerWithTransAndEnv<Subject, State, Transition, Environment>()
	, GenerativeSetStateControllerWithTransAndEnv<Subject, State, Transition, Environment>
{


	protected abstract fun initSubject() : Subject


	override fun open (environment: Environment) : Subject {
		return implementOpen(
			initSubject = ::initSubject,
			environment = environment
		)
	}


	override fun openWithState (state: State, environment: Environment) : Subject {
		val subject = open(environment)
		onOpenWithState(state)
		return subject
	}


}