package ic.design.control.gen


import ic.design.control.BaseStatefulControllerWithEnv


abstract class BaseGenerativeStatefulControllerWithEnv<Subject, State, Environment>
	: BaseStatefulControllerWithEnv<Subject, State, Environment>()
	, GenerativeStatefulControllerWithEnv<Subject, State, Environment>
{


	protected abstract fun initSubject() : Subject


	override fun open (environment: Environment) : Subject {
		return implementOpen(
			initSubject = ::initSubject,
			environment = environment
		)
	}


	override fun openWithState (state: State, environment: Environment) : Subject {
		val subject = open(environment)
		onOpenWithState(state)
		return subject
	}


}