package ic.design.control.gen


import ic.design.control.ControllerWithEnv


interface GenerativeControllerWithEnv<Subject, Environment>
	: GenerativeStatefulControllerWithEnv<Subject, Unit, Environment>
	, ControllerWithEnv<Subject, Environment>
{

	override fun openWithState (state: Unit, environment: Environment) : Subject {
		return open(environment)
	}

}