package ic.design.control.gen


import ic.design.control.SetStateControllerWithEnv


interface GenerativeSetStateControllerWithEnv
	<Subject, State, Env>
	: GenerativeSetStateControllerWithTransAndEnv<Subject, State, Unit, Env>
	, SetStateControllerWithEnv<Subject, State, Env>
{



}