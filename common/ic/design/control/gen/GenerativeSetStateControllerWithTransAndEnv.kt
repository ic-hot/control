package ic.design.control.gen


import ic.design.control.SetStateControllerWithTransAndEnv


interface GenerativeSetStateControllerWithTransAndEnv
	<Subject, State, Transition, Environment>
	: GenerativeStatefulControllerWithEnv<Subject, State, Environment>
	, SetStateControllerWithTransAndEnv<Subject, State, Transition, Environment>
{



}