package ic.design.control.gen


import ic.design.control.StatefulControllerWithEnv


interface GenerativeStatefulControllerWithEnv<Subject, State, Environment>
	: StatefulControllerWithEnv<Subject, State, Environment>
{


	fun open (environment: Environment) : Subject

	fun openWithState (state: State, environment: Environment) : Subject


}