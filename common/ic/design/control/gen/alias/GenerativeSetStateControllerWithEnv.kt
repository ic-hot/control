package ic.design.control.gen.alias


import ic.design.control.gen.GenerativeSetStateControllerWithTransAndEnv


typealias GenerativeSetStateControllerWithEnv<Subject, State, Env> =
	GenerativeSetStateControllerWithTransAndEnv<Subject, State, Unit, Env>
;