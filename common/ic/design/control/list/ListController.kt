package ic.design.control.list


import ic.ifaces.lifecycle.closeable.Closeable
import ic.ifaces.pausable.Pausable
import ic.ifaces.stateful.SetState
import ic.struct.list.List


interface ListController<Item> : SetState<List<out Item>>, Pausable, Closeable {

}