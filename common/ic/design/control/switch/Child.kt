package ic.design.control.switch


import ic.design.control.SetStateControllerWithEnv
import ic.design.control.switch.impl.isActive
import ic.ifaces.condition.Condition
import ic.ifaces.stateful.ext.state


abstract class Child
	<ParentSubject, ParentState, ParentEnv, CastedParentState: ParentState, ChildSubject: Any, ChildState, ChildEnv: Any>
	: Condition
{


	protected abstract val parent : SwitchControllerWithEnv<ParentSubject, ParentState, ParentEnv>


	abstract fun castParentState (parentState: ParentState) : CastedParentState

	abstract fun initController() : SetStateControllerWithEnv<ChildSubject, ChildState, ChildEnv>

	abstract fun initSubject (parentSubject: ParentSubject, isActiveCondition: Condition) : ChildSubject

	abstract fun initEnvironment (parentEnv: ParentEnv) : ChildEnv

	abstract fun getChildState (parentState: CastedParentState) : ChildState

	abstract fun getParentState (oldParentState: CastedParentState, childState: ChildState) : CastedParentState

	abstract fun toClose (newParentState: ParentState) : Boolean


	override val booleanValue : Boolean get() = isActive(parent.state)


	private var childSubjectField : ChildSubject? = null

	private val childSubject : ChildSubject get() {
		if (childSubjectField == null) {
			childSubjectField = initSubject(parentSubject = parent.subject, isActiveCondition = this)
		}
		return childSubjectField!!
	}


	private var childEnvironmentField : ChildEnv? = null

	private val childEnvironment : ChildEnv get() {
		if (childEnvironmentField == null) {
			childEnvironmentField = initEnvironment(parentEnv = parent.environment)
		}
		return childEnvironmentField!!
	}


	private var controllerField : SetStateControllerWithEnv<ChildSubject, ChildState, ChildEnv>? = null

	val openController : SetStateControllerWithEnv<ChildSubject, ChildState, ChildEnv>
		get() {
		@Suppress("UNCHECKED_CAST")
		if (controllerField == null) controllerField = initController()
		controllerField!!.openIfNeeded(
			subject = childSubject,
			environment = childEnvironment
		)
		return controllerField!!
	}


	fun closeControllerIfNeeded() {
		controllerField?.closeIfNeeded()
	}


}