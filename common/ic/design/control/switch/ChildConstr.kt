package ic.design.control.switch


import ic.design.control.SetStateControllerWithEnv
import ic.ifaces.condition.Condition


inline fun
	<ParentSubject, ParentState, ParentEnv, CastedParentState: ParentState, ChildSubject: Any, ChildState, ChildEnv: Any>
	SwitchControllerWithEnv<ParentSubject, ParentState, ParentEnv>.Child
(

	crossinline castParentState : (parentState: ParentState) -> CastedParentState,
	crossinline initChildController : () -> SetStateControllerWithEnv<ChildSubject, ChildState, ChildEnv>,
	crossinline initChildSubject : (parentSubject: ParentSubject, isActiveCondition: Condition) -> ChildSubject,
	crossinline initChildEnvironment : (parentEnvironment: ParentEnv) -> ChildEnv,
	crossinline getChildState : (parentState: CastedParentState) -> ChildState,

	crossinline getParentState : (oldParentState: CastedParentState, childState: ChildState) -> CastedParentState
		= { oldParentState, _ -> oldParentState }
	,

	crossinline toClose : (newParentState: ParentState) -> Boolean = { true }

) : Child<ParentSubject, ParentState, ParentEnv, ParentState, *, *, *> {

	val parent = this

	return object : Child<ParentSubject, ParentState, ParentEnv, ParentState, ChildSubject, ChildState, ChildEnv>() {

		override val parent get() = parent

		override fun castParentState (parentState: ParentState) = castParentState(parentState)

		override fun initController() = initChildController()

		override fun initSubject (parentSubject: ParentSubject, isActiveCondition: Condition) : ChildSubject {
			return initChildSubject(parentSubject, isActiveCondition)
		}

		override fun initEnvironment (parentEnv: ParentEnv) = initChildEnvironment(parentEnv)

		override fun getChildState (parentState: ParentState) : ChildState {
			@Suppress("UNCHECKED_CAST")
			return getChildState(parentState as CastedParentState)
		}

		override fun getParentState (oldParentState: ParentState, childState: ChildState) : CastedParentState {
			@Suppress("UNCHECKED_CAST")
			return getParentState(oldParentState as CastedParentState, childState)
		}

		override fun toClose (newParentState: ParentState) = toClose(newParentState)

	}

}