package ic.design.control.switch


import ic.design.control.BaseSetStateControllerWithEnv
import ic.design.control.switch.impl.isActive
import ic.ifaces.stateful.ext.state
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.collection.ext.reduce.find.findOrNull


abstract class SwitchControllerWithEnv<Subject, State, Env> : BaseSetStateControllerWithEnv<Subject, State, Env>() {


	protected abstract fun initChildren() : Collection<Child<Subject, State, Env, State, *, *, *>>

	@Suppress("LeakingThis")
	private val children = initChildren()


	override fun onSetState (state: State) {
		children.breakableForEach { child ->
			if (child.isActive(parentState = state)) {
				child.openController.state = child.getChildState(parentState = state)
			}
		}
		children.breakableForEach { child ->
			if (!child.isActive(parentState = state)) {
				if (child.toClose(state)) {
					child.closeControllerIfNeeded()
				}
			}
		}
	}

	override fun snapshotState() : State {
		val state = super.snapshotState()
		val child = children.findOrNull { child -> child.isActive(parentState = state) }
		if (child == null) return state
		@Suppress("UNCHECKED_CAST")
		child as Child<Subject, State, Env, State, Any, Any?, Any>
		return child.getParentState(oldParentState = state, childState = child.openController.state)
	}


	override fun onClose() {
		children.breakableForEach { child ->
			child.closeControllerIfNeeded()
		}
	}


}