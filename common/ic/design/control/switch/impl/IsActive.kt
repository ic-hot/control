package ic.design.control.switch.impl


import ic.design.control.switch.Child


internal fun
	<ParentState, CastedParentState: ParentState>
	Child<*, ParentState, *, CastedParentState, *, *, *>
	.isActive(
		parentState : ParentState
	)
	: Boolean
{

	try {
		castParentState(parentState)
		return true
	} catch (t: ClassCastException) {
		return false
	}

}