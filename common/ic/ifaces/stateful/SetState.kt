package ic.ifaces.stateful


@Deprecated("Use HasSettableState")
interface SetState<State> : SetStateWithTransition<State, Unit> {

	fun setState (state: State)

	override fun setState (state: State, transition: Unit) {
		setState(state)
	}

}