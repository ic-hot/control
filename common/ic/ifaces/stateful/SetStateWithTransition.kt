package ic.ifaces.stateful


interface SetStateWithTransition<State, Transition> : Stateful<State> {

	fun setState (state: State, transition: Transition)

}