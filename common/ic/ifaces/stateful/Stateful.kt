package ic.ifaces.stateful


interface Stateful<State> {

	fun getState() : State

}