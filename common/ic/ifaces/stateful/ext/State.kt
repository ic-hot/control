package ic.ifaces.stateful.ext


import ic.ifaces.stateful.SetStateWithTransition
import ic.ifaces.stateful.Stateful


inline val <State> Stateful<State>.state get() = getState()


inline var <State> SetStateWithTransition<State, Unit>.state
	get() = getState()
	set(value) = setState(value, transition = Unit)
;