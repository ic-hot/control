package ic.ifaces.stateful.ext


import ic.ifaces.stateful.Stateful
import ic.struct.value.Val


@Deprecated("To remove")
inline val <State> Stateful<State>.stateVal get() = Val { state }