package ic.util.recycler.list


import ic.base.primitives.int64.asInt32
import ic.design.control.gen.alias.GenerativeSetStateControllerWithEnv
import ic.ifaces.indexable.int32.SetInt32Index
import ic.ifaces.pausable.Pausable
import ic.ifaces.stateful.ext.state
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.list.ext.foreach.breakableForEachIndexed
import ic.struct.list.ext.reduce.find.atLeastOne
import ic.struct.map.finite.ext.foreach.breakableForEach
import ic.struct.map.incomplete.ext.containsKey
import ic.util.recycler.Recycler


abstract class RecyclingListControllerWithEnv<SubjectItem, Item, Env>
	: ic.design.control.BaseSetStateControllerWithEnv<EditableList<SubjectItem>, List<out Item>, Env>()
	, Pausable
{


	protected abstract fun generateItemController()
		: GenerativeSetStateControllerWithEnv<SubjectItem, Item, Env>
	;


	private inner class Entry {
		val itemController = generateItemController()
		val subjectItem = itemController.open(environment)
	}


	private val entries = Recycler<Item, Entry>(
		generateValue = { Entry() },
		onSeize = { _, _ -> },
		onRelease = { entry ->
			val itemController = entry.itemController
			if (itemController is Pausable) {
				itemController.pause()
			}
		},
		onClose = { entry ->
			entry.itemController.close()
		}
	)


	private fun fullyRefillSubject (state: List<out Item>) {
		subject.empty()
		state.breakableForEachIndexed { index, item ->
			val entry = entries[item]
			val itemController = entry.itemController
			if (itemController is SetInt32Index) {
				itemController.index = index.asInt32
			}
			itemController.state = item
			if (itemController is Pausable) {
				itemController.resume()
			}
			try {
				subject.add(entry.subjectItem)
			} catch (t: Throwable) {
				throw RuntimeException(
					"Unable to add item ${ entry.itemController } -> ${ entry.subjectItem }",
					t
				)
			}
		}
		orderedItems = state
		entries.recycle(
			isInUse = { item ->
				state.atLeastOne { it == item }
			}
		)
	}

	private var orderedItems : List<out Item> = List()

	private fun updateAllSubjectItems (newState: List<out Item>) {
		orderedItems.breakableForEachIndexed { index, item ->
			val entry = entries[item]
			val itemController = entry.itemController
			itemController.state = newState[index]
		}
	}

	override fun onTransitState (oldState: List<out Item>?, newState: List<out Item>) {
		if (oldState != null && newState.length == oldState.length) {
			updateAllSubjectItems(newState)
		} else {
			fullyRefillSubject(newState)
		}
	}


	override fun resume() {
		entries.used.breakableForEach { _, entry ->
			val itemController = entry.subjectItem
			if (itemController is Pausable) {
				itemController.resume()
			}
		}
	}

	override fun pause() {
		entries.used.breakableForEach { _, entry ->
			val itemController = entry.subjectItem
			if (itemController is Pausable) {
				itemController.pause()
			}
		}
	}


	override fun snapshotState() : List<Item> {
		return orderedItems.copyConvert { item ->
			if (entries.used.containsKey(item)) {
				entries[item].itemController.state
			} else {
				item
			}
		}
	}


	override fun onClose() {
		subject.empty()
		entries.close()
		orderedItems = List()
	}


}